/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

/**
 *
 * @author AdrianViveros
 */
public class Loading extends JPanel {
    int progreso=0;
    
    public void ActualizarProgreso(int progreso_valor){
        progreso = progreso_valor;
    }
    
    public void paintComponent(Graphics g){ //Metodo que pone la imagen fondo del Panel
        Dimension tam = this.getSize();
        ImageIcon imagen = new ImageIcon(getClass().getResource("/imagenes/CasinoOpen1.jpg"));
        /*En la siguiente linea por alguna rara razon le moví los parametros 2 y 3 y fue como 
        consegui que se hiciera "transparente" el panel, pero los valores predeterminados son 0
        en ambos parametros*/
        g.drawImage(imagen.getImage(), 100, 100, tam.width, tam.height, null);
        this.setOpaque(false); // Para dar ese toque de "transparencia"
        super.paintComponent(g);
        
    }
    
    
    @Override
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.translate(this.getWidth()/2, this.getHeight()/2);
        g2.rotate(Math.toRadians(270));
        Arc2D.Float arc = new Arc2D.Float(Arc2D.PIE);
        Ellipse2D circulo = new Ellipse2D.Float(0,0,30,30);//Los parametros hace que se ajuste el tamaño del circulo
        arc.setFrameFromCenter(new Point(0,0), new Point (40,40));//Tamaño del circulo rojo
        circulo.setFrameFromCenter(new Point(0,0), new Point (30,30));//pone el circulo al centro del panel
        arc.setAngleStart(1);
        arc.setAngleExtent(-progreso*3.6);//360/100=3.6
        g2.setColor(Color.red);
        g2.draw(arc);
        g2.fill(arc);
        g2.setColor(Color.WHITE);
        g2.draw(circulo);
        g2.fill(circulo);
        g2.setColor(Color.red);
        g2.rotate(Math.toRadians(90));
        g.setFont(new Font("Verdana",Font.PLAIN,15));
        FontMetrics fm = g2.getFontMetrics();
        Rectangle2D r = fm.getStringBounds(progreso+"%", g);
        int x=(0-(int)r.getWidth())/2;
        int y=(0-(int)r.getHeight())/2+fm.getAscent();
        g2.drawString(progreso+"%", x, y);
    }
    
}

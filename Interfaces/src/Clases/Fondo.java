/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author AdrianViveros
 */
public class Fondo extends JPanel{
    String Ruta;
    public void RutaImagen(String path){
        Ruta=path;
    }
    
    public void paintComponent(Graphics g){ //Metodo que pone la imagen fondo del Panel
        Dimension tam = this.getSize();
        ImageIcon imagen = new ImageIcon(getClass().getResource(Ruta));
        /*En la siguiente linea por alguna rara razon le moví los parametros 2 y 3 y fue como 
        consegui que se hiciera "transparente" el panel, pero los valores predeterminados son 0
        en ambos parametros*/
        g.drawImage(imagen.getImage(), 0, 0, tam.width, tam.height, null);
        this.setOpaque(false); // Para dar ese toque de "transparencia"
        super.paintComponent(g);
    }
    
}

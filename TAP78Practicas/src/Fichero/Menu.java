/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fichero;

import GeneradorNumeros.Generador;
import Imitador.Espejo;
import MiniEncuesta.Encuesta;
import Peliculas.Peliculas;
import Saludador.Saludador;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author adria
 */
public class Menu extends javax.swing.JFrame {

    /**
     * Creates new form Menu
     */
    public Menu() {
        initComponents();
        this.setTitle("Menu");
        this.setLocation(500, 200);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRuta = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mCortar = new javax.swing.JMenuItem();
        mCopiar = new javax.swing.JMenuItem();
        mPegar = new javax.swing.JMenuItem();
        mEliminar = new javax.swing.JMenuItem();
        mOtro = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        mGenerador = new javax.swing.JMenuItem();
        mImitador = new javax.swing.JMenuItem();
        mEncuesta = new javax.swing.JMenuItem();
        mPeliculas = new javax.swing.JMenuItem();
        mSaludador = new javax.swing.JMenuItem();
        mFichero = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jMenu1.setText("File");

        jMenuItem2.setText("Abrir...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem1.setText("Salir...");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Editar");

        mCortar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        mCortar.setText("Cortar");
        mCortar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCortarActionPerformed(evt);
            }
        });
        jMenu2.add(mCortar);

        mCopiar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        mCopiar.setText("Copiar");
        mCopiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCopiarActionPerformed(evt);
            }
        });
        jMenu2.add(mCopiar);

        mPegar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        mPegar.setText("Pegar");
        mPegar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mPegarActionPerformed(evt);
            }
        });
        jMenu2.add(mPegar);

        mEliminar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        mEliminar.setText("Eliminar");
        jMenu2.add(mEliminar);

        mOtro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        mOtro.setText("Otro");
        jMenu2.add(mOtro);

        jMenu3.setText("jMenu3");
        jMenu2.add(jMenu3);

        jMenuBar1.add(jMenu2);

        jMenu4.setText("Practicas");

        mGenerador.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        mGenerador.setText("Generador de NUmero");
        mGenerador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mGeneradorActionPerformed(evt);
            }
        });
        jMenu4.add(mGenerador);

        mImitador.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        mImitador.setText("Imitador");
        mImitador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mImitadorActionPerformed(evt);
            }
        });
        jMenu4.add(mImitador);

        mEncuesta.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        mEncuesta.setText("Encuesta");
        mEncuesta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mEncuestaActionPerformed(evt);
            }
        });
        jMenu4.add(mEncuesta);

        mPeliculas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        mPeliculas.setText("Peliculas");
        mPeliculas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mPeliculasActionPerformed(evt);
            }
        });
        jMenu4.add(mPeliculas);

        mSaludador.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        mSaludador.setText("Saludador");
        mSaludador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mSaludadorActionPerformed(evt);
            }
        });
        jMenu4.add(mSaludador);

        mFichero.setText("Fichero Path");
        mFichero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mFicheroActionPerformed(evt);
            }
        });
        jMenu4.add(mFichero);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jRuta, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jRuta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        JFileChooser fc = new JFileChooser();
        //Escribimos el nombre del titulo
        fc.setDialogTitle("Elige un fichero");
        //Indicamos que solo se puedan elegir ficheros
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        //Creamos un filtro para JFileChooser
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.txt", "txt");
        fc.setFileFilter(filtro);
        int eleccion = fc.showSaveDialog(this);
        if (eleccion == JFileChooser.APPROVE_OPTION) {
            jRuta.setText(fc.getSelectedFile().getPath());
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void mCopiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mCopiarActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(this, "Se selecciono la opcion de copiar");
    }//GEN-LAST:event_mCopiarActionPerformed

    private void mCortarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mCortarActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(this, "Se selecciono la opcion de cortar");
    }//GEN-LAST:event_mCortarActionPerformed

    private void mPegarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mPegarActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(this, "Se selecciono la opcion de Pegar");
    }//GEN-LAST:event_mPegarActionPerformed

    private void mGeneradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mGeneradorActionPerformed
        // TODO add your handling code here:
        Generador a = new Generador();
        a.setVisible(true);
    }//GEN-LAST:event_mGeneradorActionPerformed

    private void mImitadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mImitadorActionPerformed
        // TODO add your handling code here:
        Espejo espejo = new Espejo();
        espejo.setVisible(true);
    }//GEN-LAST:event_mImitadorActionPerformed

    private void mEncuestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mEncuestaActionPerformed
        // TODO add your handling code here:
        Encuesta encuesta = new Encuesta();
        encuesta.setVisible(true);
    }//GEN-LAST:event_mEncuestaActionPerformed

    private void mPeliculasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mPeliculasActionPerformed
        // TODO add your handling code here:
        Peliculas peliculas = new Peliculas();
        peliculas.setVisible(true);
    }//GEN-LAST:event_mPeliculasActionPerformed

    private void mSaludadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mSaludadorActionPerformed
        // TODO add your handling code here:
        Saludador saludador = new Saludador();
        saludador.setVisible(true);
    }//GEN-LAST:event_mSaludadorActionPerformed

    private void mFicheroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mFicheroActionPerformed
        // TODO add your handling code here:
        FicheroBoton fichero = new FicheroBoton();
        fichero.setVisible(true);
    }//GEN-LAST:event_mFicheroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JTextField jRuta;
    private javax.swing.JMenuItem mCopiar;
    private javax.swing.JMenuItem mCortar;
    private javax.swing.JMenuItem mEliminar;
    private javax.swing.JMenuItem mEncuesta;
    private javax.swing.JMenuItem mFichero;
    private javax.swing.JMenuItem mGenerador;
    private javax.swing.JMenuItem mImitador;
    private javax.swing.JMenuItem mOtro;
    private javax.swing.JMenuItem mPegar;
    private javax.swing.JMenuItem mPeliculas;
    private javax.swing.JMenuItem mSaludador;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AdrianViveros
 */
public class EjThreads1 {
    public static void main(String[] args){
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MiThread t1 = new MiThread("t1");                
                MiThread t2 = new MiThread("t2");
                t1.run();                               
                t2.run();
            }
        });
    }
    
}

class MiThread extends Thread implements Runnable{
    String nombre = "";
     
     MiThread(String nombre){
         this.nombre = nombre;
     }
    
     public void run(){
         for(int i = 0;i<100;i++){
             System.out.printf("%s, Contador: %d\n",nombre, i);
             try {
                 Thread.sleep(100);
             } catch (InterruptedException ex) {
                 Logger.getLogger(MiThread.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
     }     
}
